import React from "react";
import { TouchableHighlight, View, Image, StyleSheet } from "react-native";

import imgPlay from "../../../assets/play-48x48.png";
import imgPause from "../../../assets/pause-48x48.png";

/*
TouchableHighlight: al mantener presionado el componente cambia de color.
TouchableOpacity, al mantener presionado, el componente se hace un poco transparente (baja su opacidad).
TouchableWithoutFeedback, No tiene efectos visuales al mantener presionado el botón.
La propiedad onPress para realizar eventos en ReactNative solo funciona con los componentes Touchable.
La propiedad underlayColor cambia el color que se va a cambiar al mantener presionado el componente TouchableHighlight.
*/

const PlayPauseUI = props => (
  <TouchableHighlight
    onPress={props.onPress}
    style={styles.container}
    underlayColor="red"
    hitSlop={{
      left: 5,
      top: 5,
      bottom: 5,
      right: 5
    }}
  >
    {props.paused ? (
      <View style={styles.button}>
        <Image style={styles.icon} source={imgPlay} />
      </View>
    ) : (
      <View style={styles.button}>
        <Image style={styles.icon} source={imgPause} />
      </View>
    )}
  </TouchableHighlight>
);

const styles = StyleSheet.create({
  container: {
    backgroundColor: "grey",
    borderRadius: 20,
    overflow: "hidden"
  },
  button: {},
  icon: {
    width: 36,
    height: 36
  }
});

export default PlayPauseUI;
