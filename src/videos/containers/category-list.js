import React, { Component } from "react";
import { FlatList, Text } from "react-native";
import Layout from "../components/category-list-layout";
import Empty from "../components/empty";
import Separator from "../../sections/components/horizontal-separator";
import Category from "../components/category";

class CategoryList extends Component {
  keyExtractor = (item) => item.id.toString()
  renderEmtpy = () => <Empty text="No hay sugerencias" />;
  itemSeparator = () => <Separator />;
  renderItem = ({ item }) => {
    return <Category {...item} />;
  };
  render() {    
    return (
      <Layout
        title="Categorías">
        <FlatList
          horizontal
          keyExtractor={this.keyExtractor}
          data={this.props.list}
          ListEmptyComponent={this.renderEmtpy}
          ItemSeparatorComponent={this.itemSeparator}
          renderItem={this.renderItem}
        />
      </Layout>      
    );
  }
}

export default CategoryList;
