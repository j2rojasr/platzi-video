import React, { Component } from "react";
import { View, Text, Image, StyleSheet, ImageBackground } from "react-native";

function Category(props) {
  let genre = props.genres ? props.genres[0] : "No category";

  return (
    <ImageBackground
      style={styles.wrapper}
      source={{
        uri: props.background_image
      }}
    >
      <Text style={styles.genre}>{genre}</Text>
    </ImageBackground>
  );
}

/*
  https://facebook.github.io/react-native/docs/platform-specific-code
  
  Investigar sobre uso de sombras tanto par Android e iOS.

  Sombras paras iOS

  genre: {
    color: "white",
    fontSize: 40,
    fontWeight: 'bold',
    textShadowColor: 'rgba(0,0,0, .75)', // color de la sombra
    textShadowOffset: { // mover la sombra de manera vertical y horizontal
      width: 2,
      height: 2
    },
    textShadowRadius: 0 // difuminar
  }
 */

const styles = StyleSheet.create({
  wrapper: {
    width: 250,
    height: 100,
    borderRadius: 10,
    overflow: 'hidden',
    alignItems: 'center',
    justifyContent: 'center'
  },
  genre:{
    color: 'white',
    fontSize: 40,
    fontWeight: 'bold',
    textShadowColor: 'rgba(0,0,0, 0.60)',
    textShadowOffset: { width: 1, height: 4 },
    textShadowRadius: 5,
    //elevation: 2 //para android
  }
});

export default Category;
