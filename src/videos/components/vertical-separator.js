import React from "react";
import { View, Text, StyleSheet } from "react-native";

function VerticalSeparator(props) {
  return (
    <View
      // hacer la asignación de color más dinámico
      style={[
        styles.separator,
        {
          borderTopColor: props.color ? props.color : "#eaeaea"
        }
      ]}
    />
  );
}

const styles = StyleSheet.create({
  separator: {
    borderTopWidth: 1
  }
});

export default VerticalSeparator;
